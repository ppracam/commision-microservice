//package com.example;
//
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.test.context.ActiveProfiles;
//import org.springframework.test.context.DynamicPropertyRegistry;
//import org.springframework.test.context.DynamicPropertySource;
//import org.testcontainers.containers.PostgreSQLContainer;
//import org.testcontainers.junit.jupiter.Container;
//import org.testcontainers.junit.jupiter.Testcontainers;
//
//@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
//@ActiveProfiles("test")
//@Testcontainers
//public abstract class AbstractIntegrationTests {
//
//    @Container
//    public static PostgreSQLContainer postgreSQL =
//            new PostgreSQLContainer("postgres:latest")
//                    .withUsername("name")
//                    .withPassword("pass")
//                    .withDatabaseName("turnover");
//
//    static {
//        postgreSQL.start();
//    }
//
//    @DynamicPropertySource
//    static void postgresqlProperties(DynamicPropertyRegistry registry) {
//        registry.add("db_url", postgreSQL::getJdbcUrl);
//        registry.add("db_username", postgreSQL::getUsername);
//        registry.add("db_password", postgreSQL::getPassword);
//    }
//}
//
